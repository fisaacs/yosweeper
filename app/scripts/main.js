/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        bootstrap: '../bower_components/sass-bootstrap/dist/js/bootstrap',
        moment: '../bower_components/moment/moment'
    }
});

require([
    'backbone',
    'models/game',
    'views/game'
], function (Backbone, GameModel, GameView) {
    var game = new GameView({
        el: "#content"
    });
    window.game = game;
    game.startNewGame();

    Backbone.history.start();
});
