/*global define*/

define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    'use strict';

    var CellView = Backbone.View.extend({
        tagName: 'div',
        events: {
            'click': 'onRightClick',
            'contextmenu': 'onLeftClick'
        },
        initialize: function (options) {
            var self = this;

            // toggle flagged
            this.model.on('change:flagged', function (model) {
                self.onFlagged(model);
            });

            // on cell revealed
            this.model.on('change:visible', function (model) {
                console.log('view got model visible change');
                self.onRevealed(model);
            });

        },
        render: function () {
            this.$el.addClass('cell');
            return this;
        },
        onRightClick: function (event) {

            if (!this.model.isRevealed() && !this.model.isFlagged()) {
                this.model.reveal();
            }
        },
        onLeftClick: function (event) {
            event.preventDefault();
            if (!this.model.isRevealed()) {
                this.model.toggleFlag();
            }
        },
        onFlagged: function (model) {
            console.log('change:flagged', model.attributes);
            if (model.isFlagged()) {
                this.$el.html('<i class="fa fa-flag"></i>').addClass('flagged');
            } else {
                this.$el.html('').removeClass('flagged');
            }
        },
        onRevealed: function (model) {
            this.$el.addClass('cleared');
            if (model.hasMine()) {
                this.$el.html('<i class="fa fa-asterisk"></i>').addClass('boom');
            } else {
                var minesNear = model.get('minesNear');
                if (minesNear > 0) {
                    this.$el.html(minesNear);
                }
            }
        },
        destroy: function () {
            this.remove();
            this.unbind();
            this.model.off();
            this.model.destroy();
        }
    });

    return CellView;
});
