/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'models/game',
    'views/cell',
    'moment',
    'templates'
], function ($, _, Backbone, Game, CellView, moment, JST) {
    'use strict';

    var GameView = Backbone.View.extend({
        template: JST['app/scripts/templates/game.ejs'],
        events: {
            'click #new-sm-game': function (event) {
                event.preventDefault();
                this.startNewGame(9, 9, 10);
            },
            'click #new-med-game': function (event) {
                event.preventDefault();
                this.startNewGame(16, 16, 40);
            },
            'click #new-lg-game': function (event) {
                event.preventDefault();
                this.startNewGame(16, 24, 60);
            }
        },
        views: [],
        game: null,
        initialize: function () {
        },
        startNewGame: function (rows, columns, mines) {
            // destroy the old game
            if (!_.isNull(this.game)) {
                _.forEach(this.views, function (view) {
                    view.destroy();
                });
                this.game.off(null, null, this);
                this.game.quit();
                this.game.unlink();
                this.game.destroy();

            }

            // start the new game
            this.game = new Game({rows: rows, columns: columns, mines: mines});
            this.game.on('change:minesLeft', this.updateMinesLeft, this);
            this.game.on('change:elapsedTime', this.updateTimer, this);

            this.game.start();
            this.render();
        },
        render: function () {
            var self = this;
            var columns = self.game.get('columns');
            var rows = self.game.get('rows');

            self.$el.html(self.template());

            var $board = self.$("#board");
            $board.removeAttr('class');

            switch (columns) {
                case 24:
                    $board.addClass('board-lg');
                    break;
                case 16:
                    $board.addClass('board-md');
                    break;
                default:
                    $board.addClass('board-sm');
                    break;
            }

            this.game.cells.forEach(function (model, index) {
                var cell = new CellView({model: model});
                self.views.push(cell);
                $board.append(cell.render().$el);
                if ((index + 1) % columns == 0) {
                    $board.append('<div class="clearfix"></div>');
                }
            });

            this.updateMinesLeft(this.game);
        },
        updateMinesLeft: function (model) {
            this.$("#mines-left").html(model.get('minesLeft'));
        },
        updateTimer: function(model) {
            var elapsed = model.get('elapsedTime');

            var minutes = moment.duration(elapsed).minutes();
            var seconds = moment.duration(elapsed).seconds();

            if(minutes < 10) {
                minutes = "0" + minutes;
            }

            if(seconds < 10) {
                seconds = "0" + seconds;
            }

            var html = minutes + ":" + seconds;

            this.$("#timer").html( html );
        }
    });

    return GameView;
});
