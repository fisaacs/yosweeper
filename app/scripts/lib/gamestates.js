/**
 * Created by fred on 2/7/14.
 */
define(function () {
    return {
        NOT_STARTED: "NOT_STARTED",
        RUNNING: "RUNNING",
        WON: "WON",
        LOST: "LOST",
        QUIT: 'QUIT',
        PAUSED: 'PAUSED'
    };
});