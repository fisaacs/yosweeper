/**
 * Created by fred on 2/7/14.
 */
define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
	'use strict';

    var CellModel = Backbone.Model.extend({
        defaults: {
            x: null,
            y: null,
            flagged: false,
            hasMine: false,
            visible: false,
            minesNear: 0
        },
        hasMine: function () {
            return this.get('hasMine');
        },
        plantMine: function () {
            this.set('hasMine', true);
            return this;
        },
        isFlagged: function () {
            return this.get('flagged');
        },
        toggleFlag: function () {
            if (this.isFlagged()) {
                this.set('flagged', false);
            } else {
                this.set('flagged', true);
            }
            return this;
        },
        isRevealed: function () {
            return this.get('visible');
        },
        reveal: function () {
            this.set('visible', true);
        }
    });

    return CellModel;
});