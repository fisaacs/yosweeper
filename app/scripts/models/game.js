/*global define*/

/**
 * Created by fred on 2/7/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'lib/gamestates',
    'models/cell',
    'collections/cell'
], function ($, _, Backbone, GameStates, CellModel, CellsCollection) {
    'use strict';
    var GameModel = Backbone.Model.extend({
        defaults: {
            rows: 9,
            columns: 9,
            mines: 10,
            minesLeft: 10,
            gameState: null,
            trippedMine: null,
            startTime: 0,
            elapsedTime: 0
        },
        cells: null,
        timerId: null,
        initialize: function () {
            var self = this;
            self.cells = new CellsCollection();
            self.cells.on('change:visible', self.onCellRevealed, this);
            self.cells.on('change:flagged', self.onCellFlagged, this);
            self.on('change:gameState', self.onGameStateChanged, this);
            self.set('minesLeft', this.get('mines'));
        },
        unlink: function () {
            this.cells.off(null, null, this);
        },
        quit: function() {
            this.set('gameState', GameStates.QUIT);
        },
        onGameStateChanged: function() {
            switch(this.get('gameState')) {
                case GameStates.WON:
                case GameStates.LOST:
                    clearInterval(this.timerId);
                    break;
                case GameStates.NOT_STARTED:
                default:
                    break;
            }
        },
        /**
         * start the game
         * @param options
         */
        start: function (options) {
            this.set(options);
            this.generateCells();
            this.generateMines();
            this.set('gameState', GameStates.RUNNING);

            // start the timer
            this.set('startTime', new Date().getTime());
            var self = this;
            this.timerId = setInterval(function() {
                var start =  self.get('startTime');
                var elapsed =  new Date().getTime() - start;
                self.set('elapsedTime', elapsed);
            }, 1000);
        },
        /**
         * Generate the game board
         */
        generateCells: function () {
            var rows = this.get('rows');
            var columns = this.get('columns');

            for (var y = 0; y < rows; y++) {
                for (var x = 0; x < columns; x++) {
                    this.cells.add(new CellModel({x: x, y: y}));
                }
            }
        },
        /**
         * Plant the mines!
         */
        generateMines: function () {
            var mines = this.get('mines');

            while (mines > 0) {
                var cell = this.getRandomCell();

                // check if it has a mine
                if (!cell.hasMine()) {
                    // if not, place one, then decrement
                    cell.plantMine();
                    mines -= 1;
                    console.log('(' + cell.get('x') + ', ' + cell.get('y') + ') mine placed');
                }
            }

            var self = this;
            this.cells.forEach(function (cell) {
                var minesNear = self.countMinesNear(cell);
                cell.set('minesNear', minesNear);
            });
        },
        /**
         * Callback for cell change:visible method.
         * Should be triggered when a player clicks a cell to reveal it.
         * @param model
         */
        onCellRevealed: function (model) {
            if (model.hasMine()) {
                console.log('mine tripped!', model.attributes);
                this.set('trippedMine', model.attributes);
                this.set('gameState', GameStates.LOST);
                alert('You lose!');
            } else {
                this.countMinesLeft();

                var minesNear = this.countMinesNear(model);
                if (minesNear == 0) {
                    this.revealCellsNear(model);
                }
            }
        },
        onCellFlagged: function(model) {
            this.countMinesLeft();
        },
        /**
         * Gets a Cell by its coordinates
         * @param x
         * @param y
         * @returns {*|findWhere|findWhere|findWhere}
         */
        getCell: function (x, y) {
            var cell = this.cells.findWhere({x: x, y: y});
            return cell;
        },
        /**
         * Choose a random cell
         * @returns CellModel
         */
        getRandomCell: function () {
            var x = Math.floor(Math.random() * this.get('columns'));
            var y = Math.floor(Math.random() * this.get('rows'));
            return this.getCell(x, y);
        },
        /**
         * Gets all neighbors of a given cell
         * @param model
         * @returns {Array}
         */
        getNeighbors: function (model) {
            var self = this;
            var neighbors = []
            var x = model.get('x');
            var y = model.get('y');

            for (var i = x - 1; i <= x + 1; i++) {
                for (var j = y - 1; j <= y + 1; j++) {
                    // ignore out of bounds coordinates
                    if ((i >= 0) && (j >= 0) && (i < self.get('columns')) && (j < self.get('rows'))) {
                        neighbors.push(
                            this.cells.findWhere({x: i, y: j})
                        );
                    }
                }
            }
            return neighbors;
        },
        /**
         * Count the mines in the surrounding cells
         * @param x
         * @param y
         * @returns {number}
         */
        countMinesNear: function (model) {
            var minesFound = 0;
            var neighbors = this.getNeighbors(model);

            _.forEach(neighbors, function (cell, index) {
                if (cell.hasMine()) minesFound++;
            });


            return minesFound;
        },
        countMinesLeft: function () {
            var minesLeft = 0;
            this.cells.forEach(function (cell) {
                if (cell.hasMine() && !cell.isFlagged()) {
                    minesLeft++;
                }
            });

            // update how many mines are left
            this.set('minesLeft', minesLeft);

            // check for win condition
            if(minesLeft == 0) {
                console.log('checking win condition...');
                this.set('gameState', GameStates.WON);
                alert('You win!')
            }

            return minesLeft;
        },
        /**
         * looks at neighboring cells to see how many have mines, then reveals cell
         * @param cell
         */
        revealCellsNear: function (model) {
            var neighbors = this.getNeighbors(model);
            _.forEach(neighbors, function (cell) {
                if (!cell.isFlagged()) {
                    cell.reveal();
                }
            });
        }

    });


    return GameModel;
});
