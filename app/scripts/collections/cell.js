/*global define*/

define([
    'underscore',
    'backbone',
    'models/cell'
], function (_, Backbone, CellModel) {
    'use strict';

    var CellCollection = Backbone.Collection.extend({
        model: CellModel
    });

    return CellCollection;
});
